<?php
// $Id$

/**
 * @file
 * CKAN integration module.
 * 
 * This module integrates the CKAN api with Drupal and contains
 * implementations of hooks required to do this.
 */

/**
 * implmentation of hook_init()
 */
function ckan_init() {
  // include admin functionality
  module_load_include('inc', 'ckan', 'ckan.admin');
  
  // include api functionality
  module_load_include('inc', 'ckan', 'ckan.api');
  
  // include front-end functionality
  module_load_include('inc', 'ckan', 'ckan.pages');
  
  // include services functionality
  module_load_include('inc', 'ckan', 'ckan.services');
}

/**
 * implementation of hook_perm()
 */
function ckan_perm() {
  return array('administer ckan', 'ckan info', 'use embedded edit', 'use embedded create');
}

/**
 * implementation of hook_menu()
 */
function ckan_menu() {
  // initialise
  $items = array();

  // admin items
  $items['admin/settings/ckan'] = array(
    'title' => 'CKAN integration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ckan_settings_form'),
    'access arguments' => array('administer ckan'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/ckan/settings'] = array(
    'title' => 'CKAN integration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ckan_settings_form'),
    'access arguments' => array('administer ckan'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
 
  
  // define page callback for non-existant ckan nodes that matches any
  // current url structure. i.e. include a default path and any defined 
  // by pathauto
  $paths = _get_ckan_enabled_type_paths();

  foreach ($paths as $path => $arguments) {
    $items[$path] = array(
      'page callback' => 'ckan_on_demand_page',
      'page arguments' => $arguments,
      'file' => 'ckan.pages.inc',
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    );
  }
    
  $items['node/%node/embedded-edit'] = array(
    'title' => 'Edit',
    'page callback' => 'ckan_embedded_form',
    'page arguments' => array('edit', 1),
    'access callback' => 'ckan_access',
    'access arguments' => array(1, 'embedded_edit'),
    'type' => MENU_LOCAL_TASK,
  );
  
  // determine ckan-enabled types
  $ckan_types = variable_get('ckan_types', array());  
  
  foreach ($ckan_types as $type => $title) { 
    // define arguments to pass to form
    $form_args = array(
      '#type' => $type,
      '#title' => $title,
    );
    
    // define menu item
    $items['node/embedded-add/' . $type] = array(
      'title' => 'Create ' . $title,
      'page callback' => 'ckan_embedded_form',
      'page arguments' => array('create', $form_args),
      'access callback' => 'ckan_access',
      'access arguments' => array(NULL, 'embedded_create'),
      'type' => MENU_NORMAL_ITEM,
    );
  }  

  return $items;
}

/**
 * get an array of all paths that ckan-enabled nodes could use
 */
function _get_ckan_enabled_type_paths() {
  // initialise path array
  $paths = array();
  
  // determine ckan-enabled types
  $ckan_types = variable_get('ckan_types', array());
      
  // set default path
  $default_ckan_path = variable_get('ckan_default_base_path', 'ckan/catalogue');
  
  // add argument wildcards to path
  $default_ckan_path .= (count($ckan_types) > 1) ? '/%/%' : '/%';

  // add default to output paths
  $paths[$default_ckan_path] = _generate_path_arguments($default_ckan_path);
  
  if (module_exists('pathauto')) {
    // pathauto enabled so gather any custom node paths
    
    // determine default pathauto node path
    $default_node_path = variable_get('pathauto_node_pattern', $default_ckan_path);
    
    foreach ($ckan_types as $type) {
      // determine node path for type
      $path = variable_get("pathauto_node_{$type}_pattern", $default_node_path);
      
      // swap tokens for %
      $path = preg_replace('#\[.*?\]#', '%', $path);
      // make sure default node path is used if node path not defined
      $path = (empty($path)) ? $default_node_path : $path;
      
      // assign path and arguments to output array
      $paths[$path] = _generate_path_arguments($path);
    }
  }
  return $paths;
}
/**
 * helper function to generate argument array for menu item
 * 
 * @param string $path
 */
function _generate_path_arguments($path) {
  // intialise arguments array
  $arguments = array();  
  
  // generate an argument array that matches the number of wildcards
  for ($i=0; $i < substr_count($path, '%'); $i++) {
    $arguments[] = $i + substr_count($path, '/');
  }
  
  return $arguments;
}

/**
* access callback for using a given operation on a ckan-enabled node
*
* @param stdclass $form
*/
function ckan_access($node, $op) {
  // intialise
  $access = FALSE;
  
  // determine ckan-enabled types
  $ckan_types = variable_get('ckan_types', array());
  
  if ((!empty($node) && !empty($ckan_types[$node->type]) || empty($node))) {
    // content type is ckan-enabled
    
    $access = user_access('administer ckan');
    
    if (!$access) {
      // no admin-level access so allow modules to define access via hook
      $accumulated_access = module_invoke_all('ckan_access', $node, $op);
      
      // access is equivalent to all hook results ANDed.
      $access = !in_array(FALSE, $accumulated_access) && in_array(TRUE, $accumulated_access);
    }
  }
  
  return $access;
}

/**
 * implementation of hook_ckan_access()
 * 
 * defines default access to ckan-enabled nodes
 * 
 * @param stdclass $node
 * @param string $op
 */
function ckan_ckan_access($node, $op) {
  // intialise
  $access = FALSE;
  
  switch ($op) {
    case 'embedded_create':
      // users must have access to the 'use embedded create' permission
      $access = user_access('use embedded create');
      break;
      
    case 'embedded_edit':
      // users must have access to the 'use embedded edit' permission
      $access = user_access('use embedded edit');
      break;
  }
  
  return $access;
}

/**
 * implementation of hook_form_alter
 * 
 * @param array $form
 * @param array $form_state
 * @param string $form_id
 */
function ckan_form_alter(&$form, &$form_state, $form_id) {
  // initialise
  $node = $form['#node'];
  
  // get list of ckan-enabled types
  $ckan_types = variable_get('ckan_types', array());
  
  // determine if ckan is enabled for this type
  $ckan_enabled = !empty($ckan_types[$node->type]);
  
  if (($form['#id'] == 'node-form') && ($ckan_enabled)) {
    // form relates to ckan-enabled type
    
    // add a field for ckan package id
    $form['ckan_name'] = array(
      '#title' => t('CKAN name'),
      '#description' => t('lowercase alphanumeric and underscores only'),
      '#type' => 'textfield',
      '#default_value' => $node->ckan_dictionary['ckan_name'],
      '#maxlength' => 100,
      '#required' => TRUE,
      '#weight' => -999,
    );
  }
}

/**
 * implementation of hook_nodeapi()
 * 
 * @param stdclass $node
 * @param string $op
 * @param boolean $a3
 * @param boolean $a4
 */
function ckan_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  $ckan_types = variable_get('ckan_types', array());
  
  if (!empty($ckan_types[$node->type])) {
    // node type is ckan-enabled
    
    switch ($op) {
      case 'view':
        if ($node->ckan_dictionary['ckan_name']) {
          $node->ckan = ckan_get($node->ckan_dictionary['ckan_name']);
              
          // prepare node for theming
          ckan_prepare_content($node);
        }        
        break;
        
      case 'load':
        // attach ckan dictionary to node object 
        $node->ckan_dictionary = ckan_read_dictionary($node->nid);
            
        if ($node->ckan_dictionary['ckan_name']) {
          $node->ckan = ckan_get($node->ckan_dictionary['ckan_name']);
          
          $node->ckan_cache_info = $node->ckan->cache_info;
          unset($node->ckan->cache_info);
          
          // refresh from ckan
          ckan_refresh_node($node);
        }

        break;
        
      case 'insert':
      case 'update':
        // save ckan dictionary for this node
        ckan_write_dictionary($node);
        break;
        
      case 'delete':
        // delete the ckan dictionary for this node
        ckan_delete_dictionary($node->nid);
        break;
    }
  }
}



/**
 * implementation of hook_token_values()
 * 
 * @param string $type
 * @param stdclass $object
 * @param array $options
 */
function ckan_token_values($type, $object = NULL, $options = array()) {
  if ($type == 'node') {
    // get ckan-enabled types
    $ckan_types = variable_get('ckan_types', array()); 
    
    if (!empty($ckan_types[$object->type])) {
      // object in question is ckan-enabled
      foreach ($object->ckan_dictionary as $key => $value) {
        // create token value for each ckan dictionary item
        $tokens['ckan-dictionary-' . $key] = $value;
      }
      return $tokens; 
    }
  }
}

/**
 * implementation of hook_token_list()
 * 
 * @param string $type
 * @param stdclass $object
 * @param array $options
 */
function ckan_token_list($type = 'all') {
  if ($type == 'node' || $type == 'all') {
    // extract all currently used ckan dictionary items from the database
    $result = db_query('SELECT DISTINCT name FROM {ckan_dictionary}');
    
    while ($row = db_fetch_object($result)) {
      // create token-listing for each
      $tokens['node']['ckan-dictionary-' . $row->name] = t('A dynamic item from the node\'s ckan dictionary.');
    }
    
    return $tokens;
  }
}


/**
 * run the ckan reponse through a basic theme. not expecting this to be used
 * on production systmes as it'll look a bit rubbish!
 * 
 * @param stdclass $node
 */
function ckan_prepare_content(&$node) {
  // get theme setting
  $theming_type = variable_get('ckan_theming_type', '1');
  
  if ($theming_type == '1') {

    if (!empty($node->ckan)) {
      // theme it
      $node->content['ckan']['#value'] = theme('ckan_list', $node->ckan, $node->ckan_dictionary['values']);
    }
  }
}


/**
 * implmentation of hook_ckan_mapper()
 * 
 * maps basic ckan properties to the node
 * 
 * @param stdclass $node
 * @param stdclass $response
 */
function ckan_ckan_mapper(&$node, $ckan_response) {
  // create teaser for use in views
  $node->title = $ckan_response->title;
  $node->teaser = node_teaser($ckan_response->notes, NULL, variable_get('ckan_teaser_length', 250));
  $node->body = $ckan_response->notes;
  
  if (variable_get('ckan_use_dates', TRUE)) {
    // apply ckan date stamps to node
    $node->created = strtotime($ckan_response->metadata_created);
    $node->changed = strtotime($ckan_response->metadata_modified);
  }
}