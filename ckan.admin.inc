<?php
// $Id$

/**
 * @file
 * Admin settings
 * 
 * This file defines the admin settings and any code specifically used for this purpose
 */

/**
 * ckan admin settings form
 */
function ckan_settings_form() {
  $form = array();
  
  // GENERAL SETTINGS
  $form['fs-general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#weight' => -10,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['fs-general']['ckan_default_base_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Catalogue base path'),
    '#required' => TRUE,
    '#default_value' => variable_get('ckan_default_base_path', 'ckan/catalogue'),
    '#description' => t('Default url where ckan packages are accessible. If only one content type is ckan-enabled, this URL will be followed by the ckan package name. If multiple types are enabled, the url will be followed by type and then ckan package name.'),
  );
  
  $form['fs-general']['ckan_hash_checking_enabled'] = array(
    '#type' => 'select',
    '#title' => t('Enable hash-checking'),
    '#options' => array(
      '0' => 'No - good for ckan-related development testing',
      '1' => 'Yes - recommended for production environments',
    ),
    '#default_value' => variable_get('ckan_hash_checking_enabled', TRUE),
    '#description' => t('Hashing is used to identify changes in CKAN reponses. If no change is detect, a CKAN-enabled node will not be refreshed when hash-checking is enabled which is not so handy when developing ckan mappings, for example.'),
  );
  $form['fs-general']['ckan_theming_type'] = array(
    '#type' => 'select',
    '#title' => t('Enable theming and insert CKAN reponse into node content array'),
    '#options' => array(
      '0' => 'Disabled',
      '1' => 'Basic theming',
    ),
    '#default_value' => variable_get('ckan_theming_type', '1'),
    '#description' => t('Basic theming adds a themed list of all CKAN response properties into the node content array. Not very good looking and is not recommended for a production environment.'),
  );    
  
  
  // API SETTINGS
  $form['fs-api'] = array(
    '#type' => 'fieldset',
    '#title' => t('API settings'),
    '#weight' => -8,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );   
  $form['fs-api']['ckan_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CKAN api location'),
    '#default_value' => variable_get('ckan_api_url', 'http://www.ckan.net/'),
    '#description' => t('URL to which CKAN service requests are made'),
  );  
  
  // user/pass no longer used apparently
  
//  $form['fs-api']['ckan_htauth_user'] = array(
//    '#type' => 'textfield',
//    '#title' => t('CKAN api user'),
//    '#default_value' => variable_get('ckan_htauth_user', ''),
//    '#description' => t('Username for htauth, if used for api security.'),
//  );  
//  $form['fs-api']['ckan_htauth_pass'] = array(
//    '#type' => 'textfield',
//    '#title' => t('CKAN api password'),
//    '#default_value' => variable_get('ckan_htauth_pass', ''),
//    '#description' => t('Password for htauth, if used for api security.'),
//  );  
  $form['fs-api']['ckan_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('CKAN api key'),
    '#default_value' => variable_get('ckan_api_key', ''),
    '#required' => TRUE,
    '#description' => t('Required to use the CKAN api.'),
  );    
  
  // CONTENT TYPE SETTINGS
  $form['fs-type'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content type settings'),
    '#weight' => -5,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['fs-type']['ckan_types'] = array(
    '#type' => 'select',
    '#title' => t('CKAN-enabled content types'),
    '#multiple' => TRUE,
    '#default_value' => variable_get('ckan_types', array()),
    '#options' => node_get_types('names'),
    '#description' => t('Selected content types will have CKAN integration functionality attached to them.'),
  );
  $form['fs-type']['ckan_teaser_length'] = array(
    '#type' => 'textfield',
    '#title' => t('CKAN teaser length'),
    '#default_value' => variable_get('ckan_teaser_length', 250),
    '#required' => TRUE,
    '#description' => t('CKAN notes field is mapped to teaser. Use this number to set the truncation length. This is approximate as truncation is word-safe.'),
  );
 
  // MAPPING SETTINGS
  $form['fs-mappings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mapping settings'),
    '#weight' => -3,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['fs-mappings']['ckan_use_dates'] = array(
    '#type' => 'select',
    '#title' => t('Application of CKAN datestamps to drupal nodes'),
    '#multiple' => FALSE,
    '#default_value' => variable_get('ckan_use_dates', TRUE),
    '#options' => array(
      '1' => 'Apply CKAN datastamps to drupal nodes',
      '0' => 'Don\'t apply CKAN datestamps to drupal nodes',
    ),
    '#description' => t('If selected, CKAN datestamps will be applied to nodes. If not, the created and modified properties will reflect when the node was created inside drupal, rather than CKAN.'),
    '#weight' => -99,
  );
  
  // CACHE SETTINGS
  $form['fs-cache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache settings'),
    '#weight' => -2,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );      
  $form['fs-cache']['ckan_cache_setting'] = array(
    '#type' => 'select',
    '#title' => t('Service response cache level'),
    '#options' => array(
      '0' => 'Don\'t use any cache',
      '1' => 'Back-up cache',
      '2' => 'Advanced cache -- Not implemented yet',
    ),
    '#default_value' => variable_get('ckan_cache_setting', 1),
    '#description' => t('No cache has less resource overhead  but may result in blank screens if ckan service is down. The back-up cached version is used only if service is down.'),
  ); 

  
  // TEST SETTINGS
  $form['fs-test'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test settings'),
    '#weight' => -1,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );   
  $form['fs-test']['ckan_test_name'] = array(
    '#type' => 'textfield',
    '#title' => t('CKAN test package name'),
    '#default_value' => variable_get('ckan_test_name', ''),
    '#description' => t('This is used for service testing.'),
  );
  
  _ckan_service_test();
  
  return system_settings_form($form);
}

/**
 * use the test package name to get a response from CKAN service
 */
function _ckan_service_test() {
  $test_name = variable_get('ckan_test_name', '');
  
  if (empty($test_name)) {
    // test name not defined
    drupal_set_message(t('Set a CKAN test package name to enable service checker on this page.'));
    
  } else {
    // fetch test package title from service
    $title = ckan_get($test_name, 'title', TRUE);
    
    if (empty($title)) {
      // no response
      drupal_set_message(t('Error! CKAN service failed test.'), 'error');
      
    } else {
      // all good
      drupal_set_message(t('CKAN service test passed!'));
    }
  }
}
