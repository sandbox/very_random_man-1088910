<?php 
// $Id$
/**
 * @file
 * Theming functions 
 * 
 * This contains all functions relating to theming and building of 
 * pages for the CKAN integration module.
 * 
 */
/**
 * implementation of hook_theme
 *
 * @return array
 */
function ckan_theme() {
  return array(
    'ckan_list' => array(
      'arguments' => array('items' => array(), 'overrides' => array(), 'prefix' => ''),
    ),
  );
}

/**
 * theme ckan response recursively 
 * 
 * @param collection $items
 */
function theme_ckan_list($items, $overrides, $prefix = '') {
  $output = '';

  foreach ($items as $property => $value) {
    if (is_array($value) || is_object($value)) {
      if (!empty($value)) {
        $output .= '<dt><h3>' . $property . '</h3></dt><dd>' . theme('ckan_list', $value, $overrides, $prefix . $property . '__') . '</dd>';
      }
    } else {
      if (!empty($value)) {
        $output .= '<dt style="font-weight: bold">' . $property . '</dt><dd>' . (isset($overrides[$prefix . $property]) ? $overrides[$prefix . $property] : $value) . '</dd>';
      }
    }
  }
  
  return '<dl>' . $output . '</dl>';
}

/**
 * menu callback that will be called if a ckan-enabled url
 * is called when no node exists with that alias.
 * 
 * default example:  
 * /ckan/catalogue/package-id-goes-here  
 * 
 * ckan-enabled content type with pathauto-defined node path alias example:
 * /data/packages/package-id-goes-here
 * 
 * this will create a drupal node for a given package id and
 * view it.
 * 
 * Expects these arguments from the url:
 * one argument: ckan-id
 * two arguments: type, ckan-id
 * 
 * @param string $ckan_name
 */
function ckan_on_demand_page() {
  // get arguments
  $args = func_get_args();

  // get ckan-enabled types
  $ckan_types = variable_get('ckan_types', array());  
  
  if (empty($ckan_types)) {
    // error! no ckan-enable content types
    drupal_set_message(t('No content types have been ckan-enabled yet'), 'error');
    drupal_not_found();
    
  } else {   
    // determine ckan-id and node type from arguments
    switch (count($args)) {
      case 0:
        // no args, no dice.
        drupal_set_message(t('CKAN name missing from url'), 'error');
        break;
        
      case 1:
        // one arg: ckan-id
        $ckan_name = $args[0];
        $node_type = array_shift($ckan_types);
        break;
        
      default:
        // two args: type, ckan-id
        $ckan_name = $args[1];
        $node_type = $args[0];
        break;
    }    
    
    // create new ckan node
    if (!empty($ckan_name)) $new_node = ckan_create_node($ckan_name, $node_type, TRUE);

    if ($new_node->nid) {
      // go to the new node
      // used to have a node_view and set_title but it didn't work as cleanly
      drupal_goto('node/' . $new_node->nid);
  
    } else {
      // no nodes is bad nodes
      drupal_not_found();
    }
  }
}

function ckan_embedded_form($op, $arg = NULL) {
  // intialise 
  $output = '';

  // add ckan js and css
  _add_ckan_header_files();
  
  // get form content
  $output = ckan_get_form($op, $arg);
  
  return $output;
}

/**
 * helper function to add ckan js and css
 */
function _add_ckan_header_files() {
  drupal_add_css(drupal_get_path('module', 'ckan') . '/css/package_create.css');
  drupal_add_css(drupal_get_path('module', 'ckan') . '/css/ckan_forms.css');
  drupal_add_css(drupal_get_path('module', 'ckan') . '/css/flexitable.css');
  drupal_add_css(drupal_get_path('module', 'ckan') . '/css/buttons.css');
//  drupal_add_css(drupal_get_path('module', 'ckan') . '/css/display.css');
  drupal_add_css(drupal_get_path('module', 'ckan') . '/css/ckan.master.css');
//  drupal_add_css(drupal_get_path('module', 'ckan') . '/css/tagcomplete.css');
  drupal_add_js(drupal_get_path('module', 'ckan') . '/js/flexitable.js', 'module');
  drupal_add_js(drupal_get_path('module', 'ckan') . '/js/tagcomplete.js', 'module');
  drupal_add_js(drupal_get_path('module', 'ckan') . '/js/reveal_instructions.js', 'module'); 
}
