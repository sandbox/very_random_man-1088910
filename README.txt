Installation
------------

- download and enable module.
- 

Notes
-----

If a single type is ckan-enabled, one url argument is required: ckan-name.
If multiple types are ckan-enabled, two arguments are required: content type and ckan-name. 

Note: Flush menu cache after changing from single to multiple types and vice versa.

If path auto module is used, the node path for ckan-enabled types must have these arguments in the as 
tokens: [type] and [ckan-name]. 


To add custom mappings
----------------------

Use the following hook to either map ckan values to the node and to override or transform
values used if theming is selected in the CKAN settings.

hook_ckan_mapper(&$node, $ckan_response) {
  // code here to map properties from $ckan_response to $node
  // for example, you could use this to map a CKAN property to a taxonomy.
  
  // any values you wish to change in the standard ckan output that is appended to the node content array
  // must be returned in an array. the keys must match the field names as shown in the ckan ui settings page
  
  return $ckan_values;
}