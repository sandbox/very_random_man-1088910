<?php
// $Id$

/**
 * @file
 * Services hook implementation for CKAN integration module
 * 
 * This is a collection of Service hook implementations and callbacks to allow
 * ckan.net to update Drupal. 
 */
/*
 * Implementation of hook_disable()
 */
function ckan_disable() {
  cache_clear_all('services:methods', 'cache');
}
/*
 * Implementation of hook_enable()
 */
function ckan_enable() {
  cache_clear_all('services:methods', 'cache');
}

/**
 * implementation of hook_service()
 */
function ckan_service() {
  return array(
    // CACHE INVALIDATE
    array(
      '#method' => 'ckan.cache_invalidate',
      '#callback' => 'ckan_service_cache_invalidate',
      '#args' => array(
        array(
          '#name' => 'name',
          '#type' => 'string',
          '#optional' => FALSE,
          '#description' => t('CKAN package name.'),
        ),
      ),
      '#return' => 'boolean',
      '#help' => t('Invalidate the ckan response cache for a ckan-enabled node to ensure that the node is refreshed next time it is used.'),
    ),
    // UPDATE
    array(
      '#method' => 'ckan.update',
      '#callback' => 'ckan_service_cache_invalidate',
      '#args' => array(
        array(
          '#name' => 'name',
          '#type' => 'string',
          '#optional' => FALSE,
          '#description' => t('CKAN package name.'),
        ),
      ),
      '#return' => 'boolean',
      '#help' => t('Invalidate the ckan response cache for a ckan-enabled node to ensure that the node is refreshed next time it is used.'),
    ),
    // DELETE
    array(
      '#method' => 'ckan.delete',
      '#callback' => 'ckan_service_delete',
      '#args' => array(
        array(
          '#name' => 'name',
          '#type' => 'string',
          '#optional' => FALSE,
          '#description' => t('CKAN package name.'),
        ),
      ),
      '#return' => 'boolean',
      '#help' => t('Delete a ckan-enabled node'),
    ),
    // CREATE
    array(
      '#method' => 'ckan.create',
      '#callback' => 'ckan_service_create',
      '#args' => array(
        array(
          '#name' => 'name',
          '#type' => 'string',
          '#optional' => FALSE,
          '#description' => t('CKAN package name.'),
        ),
        array(
          '#name' => 'type',
          '#type' => 'string',
          '#optional' => TRUE,
          '#description' => t('node type'),
        ),
      ),
      '#return' => 'boolean',
      '#help' => t('Create a new a ckan-enabled node'),
    ),
  );
}

/**
 * service callback to invalidate the ckan response cache
 * 
 * @param unknown_type $name
 */
function ckan_service_cache_invalidate($ckan_name = '') {
  // remove response from cache
  cache_clear_all($ckan_name, 'cache_ckan');

  // determine nid
  $nid = ckan_get_nid($ckan_name);

  // touch node to inform search to index
  search_touch_node($nid);
}

/**
 * service callback to delete a node
 * 
 * @param unknown_type $name
 */
function ckan_service_delete($ckan_name = '') {
  // remove response from cache
  cache_clear_all($ckan_name, 'cache_ckan');

  // determine nid
  $nid = ckan_get_nid($ckan_name);
  
  // delete
  node_delete($nid);
}
/**
 * service callback to create a node
 * 
 * @param unknown_type $name
 */
function ckan_service_create($ckan_name = '', $type = '') {
  // intialise 
  // remove response from cache just in case
  cache_clear_all($ckan_name, 'cache_ckan');

  // get ckan-enabled types
  $ckan_types = variable_get('ckan_types', array());  

  if (empty($ckan_types)) {
    // no ckan-enabled content types
    $output = services_error(t('There are no CKAN-enabled content types.'), 501);
    
  } else {
    // use first content type in list if type not specified
    if (empty($type)) $type = array_shift($ckan_types); 
    
    // create new ckan node
    $new_node = ckan_create_node($ckan_name, $type, TRUE);
  }
  
}