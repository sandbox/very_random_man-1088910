<?php
// $Id$

/**
 * @file
 * An add-on for the CKAN integration module
 * 
 * A module to use hooks provided by the CKAN integration module to 
 * map CKAN service response to a tags vocabulary.
 */

/**
 * implementation of hook_form_alter()
 * 
 * @param array $form
 * @param array $form_state
 * @param string $form_id
 */
function ckan_tags_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'ckan_settings_form':
      $ckan_types = variable_get('ckan_types', array());
      
      // add a vocab select in for each ckan-enabled type
      foreach ($ckan_types as $type) {
        $form['fs-mappings']['ckan_tags_vid_' . $type] = array(
          '#type' => 'select',
          '#title' => t('Map CKAN tags for !type content to taxonomy', array('!type' => $type)),
          '#multiple' => FALSE,
          '#default_value' => variable_get('ckan_tags_vid_' . $type, ''),
          '#options' => ckan_tags_get_vocab_options($type),
          '#description' => t('If no vocabs are listed, you need to assign one in the !link', array('!link' => l('Taxonomy admin', 'admin/content/taxonomy'))),
          '#weight' => -10,
        );
      }
      break;
  }
}

/**
 * generate a list of vocab options for a select form element
 */
function ckan_tags_get_vocab_options($type) {
  $vocabs = taxonomy_get_vocabularies($type);

  $options = array();

  foreach ($vocabs as $vocab) {
    $options[$vocab->vid] = $vocab->name;
  }

  return $options;
}

/**
 * implmentation of hook_ckan_mapper()
 * 
 * maps ckan properties to the node taxonomy
 * 
 * @param stdclass $node
 * @param stdclass $response
 */
function ckan_tags_ckan_mapper(&$node, $ckan_response) {
  // get vocab id to map ckan tags to
  $vid = intval(variable_get('ckan_tags_vid_' . $node->type, ''));

  if (!empty($vid)) {
    // overwrite node tags with ckan tags
    $node->taxonomy['tags'][$vid] = implode($ckan_response->tags, ',');
  }
  
  
}