<?php 
// $Id$

/**
 * @file
 * CKAN API function
 * 
 * API to cover all CKAN integration functionality including service 
 * interrogation, caching and utility functions
 */

/**
 * display status message, dependent on access
 * 
 * @param string $msg
 * @param string $status
 */
function ckan_msg($msg, $status = 'status') {
  if (user_access('ckan info')) {
    drupal_set_message($msg, $status);
  }
}

/**
 * create a basic ckan node. refresh from ckan optional.
 * 
 * @param string $ckan_name
 * @param string $node_type
 * @param boolean $refresh
 */
function ckan_create_node($ckan_name, $node_type, $refresh = TRUE) {
  // create basic node 
  $node = (object) array(
    'title' => ckan_get($ckan_name, 'title'),
    'type' => $node_type,
    'ckan_dictionary' => array(
      'ckan_name' => $ckan_name,
      'ckan_id' => ckan_get($ckan_name, 'id'),
    ),
    'ckan' => ckan_get($ckan_name),
  );

  if (empty($node->title)) {
    // error retrieving title so it most likely doesn't exist
    // or there's been a network error
    $node = FALSE;
    
  } else {
    if ($refresh) {
      // refresh node to populate drupal with essential ckan gubbins
      ckan_refresh_node($node);
      
    } else {
      // just save it
      node_save($node);
    }
    
    ckan_msg(t('CKAN node created.'));
  }
  return $node;
}

/**
 * refresh a given node from ckan. this will populate any 
 * relevant parts of the node object with data from ckan.
 * 
 * @param stdclass $node
 */
function ckan_refresh_node(&$node) {
  // don't refresh on edit form
  if (arg(2) == 'edit') return;
  
  // for some reason node_save triggers hook_nodeapi with a load op
  // we only want one refresh to happen.
  static $in_progress;
  if (!is_array($in_progress)) $in_progress = array();
  
  if ($in_progress[$node->nid]) {
    return;
    
  } else {
    $in_progress[$node->nid] = TRUE;
  }
  
  // create hash for response
  $hash = md5(serialize($node->ckan));
  $hash_checking_enabled = variable_get('ckan_hash_checking_enabled', TRUE);
  
  if (($hash != $node->ckan_dictionary['hash'] && $hash_checking_enabled) || !$hash_checking_enabled && !$node->ckan_cache_info['#used_cached_response'] ) {
    // only refresh the node if something has changed in the response

               
    // intitialise override values if required
    if (!is_array($node->ckan_dictionary['values'])) $node->ckan_dictionary['values'] = array();   
    
    // allow modules to map any ckan response properties to the node and/or return override values
    $node->ckan_dictionary['values'] = module_invoke_all('ckan_mapper', $node, $node->ckan, $node->ckan_dictionary['values']);

    // update stored hash
    $node->ckan_dictionary['hash'] = $hash;

    ckan_msg(t('CKAN node refreshed.'));
    node_save($node);
  }  
}


/**
 * extract a package or an property of a package from ckan
 * 
 * @param string $ckan_name
 * @param string $attribute
 */
function ckan_get($ckan_name, $property = NULL, $force_cache_bypass = FALSE) {
  // get entire package
  $package = ckan_get_package($ckan_name, $force_cache_bypass);
  
  if ($package) {
    // package returned for that name
    $output = empty($property) ? $package : $package->{$property};

  } else {
    // no package returned for that name
    $output = FALSE;
  }
  
  return $output;
}

/**
 * get a nid for a given ckan_name
 * 
 * @param string $ckan_name
 */
function ckan_get_nid($ckan_name) {
  // intialise
  $nid = '';
  
  // query ckan dictionary for ckan name
  $row = db_fetch_object(db_query("SELECT nid FROM {ckan_dictionary} c WHERE c.value = '%s'", serialize($ckan_name)));
  $nid = $row->nid;
  
  return $nid;  
}

/**
 * get ckan package from ckan service
 * 
 * this is basically a caching wrapper for _get_package()
 * 
 * @param string $ckan_name
 */
function ckan_get_package($ckan_name, $force_cache_bypass = FALSE) {
  // check for empty
  if (empty($ckan_name)) return;
  
  // intialise
  static $ckan_package;
  if (!is_array($ckan_package)) $ckan_package = array();
  
  if (empty($ckan_package[$ckan_name])) {
    // no static cache
    $cache_setting = ($force_cache_bypass) ? 0 : intval(variable_get('ckan_cache_setting', 1));
    $used_cached_response = FALSE;
    
    switch ($cache_setting) {
      case 0:
        // no cache
        $ckan_package[$ckan_name] = _get_package($ckan_name);
        
        if (empty($ckan_package[$ckan_name])) {
          ckan_msg(t('No CKAN service response.'));
        } else {
          ckan_msg(t('Package retrieved from CKAN service.'));
        }
        break;
        
      case 1:
        // cache used as back-up
        $ckan_package[$ckan_name] = _get_package($ckan_name);

        if (empty($ckan_package[$ckan_name])) {
          ckan_msg(t('No CKAN service response.'));
        } else {
          ckan_msg(t('Package retrieved from CKAN service.'));
        }
        
        if (empty($ckan_package[$ckan_name])) {
          // on-demand retrieval failure so retrieve cached response
          $cached_response = cache_get($ckan_name, 'cache_ckan');
          $ckan_package[$ckan_name] = $cached_response->data;
          
          $used_cached_response = TRUE;
          
          if (empty($ckan_package[$ckan_name])) {
            ckan_msg(t('No CKAN response found in cache.'));
          } else {
            ckan_msg(t('CKAN reponse retreived from cache'));
          }
          
        } else {
          // on-demand success so cache response
          cache_set($ckan_name, $ckan_package[$ckan_name], 'cache_ckan');
          
          ckan_msg(t('CKAN reponse added to cache'));
        }
        break;
        
      case 2:
        // advanced cache -- cache used by preference
        
        // THIS REQUIRES THE SERVICES TO HAVE BEEN IMPLMENTED ON CKAN
        
        $cached_response = cache_get($ckan_name, 'cache_ckan');
        $ckan_package[$ckan_name] = $cached_response->data;
          
        if (empty($ckan_package[$ckan_name])) {
          ckan_msg(t('No CKAN response found in cache.'));
        } else {
          ckan_msg(t('CKAN reponse retreived from cache'));
        }
        
        if (empty($ckan_package[$ckan_name])) {
          // no cache so query ckan on demand 
          $ckan_package[$ckan_name] = _get_package($ckan_name);
          
          ckan_msg(t('No cached CKAN reponse so response taken from service.'));
          // cache response
          cache_set($ckan_name, $ckan_package[$ckan_name], 'cache_ckan');
          
          ckan_msg(t('CKAN reponse added to cache'));
          
        } else {
           $used_cached_response = TRUE;
        }
        break;
    }
    
    // add cache info into the object as this might be used elsewhere
    $ckan_package[$ckan_name]->cache_info = array(
      '#setting' => $cache_setting,
      '#used_cached_response' => $used_cached_response,
    );
  }
  
  return $ckan_package[$ckan_name];
}

/**
 * helper function to connect to ckan and retreive a given package
 * 
 * @param unknown_type $ckan_name
 */
function _get_package($ckan_name) {
  // intialise
  $response = FALSE;
  
  // create connector
  $ckan_connector = ckan_get_connector();

  // load ckan data
  try {
    $response = $ckan_connector->getPackage($ckan_name);
      
  } catch (Exception $e) {
    drupal_set_message(check_plain($e->getMessage()), 'error');
  }
  
  return $response;
}

/**
 * get ckan connector. statically cache for faster re=use
 */
function ckan_get_connector() {
  static $ckan_connector;
  
  if (empty($ckan_connector)) {
    // use old ckan class for time being
    // TODO replace with something better
    
    // include old ckan class 
    module_load_include('inc', 'ckan', 'ckan.classes');
    
    // get settings
    $url = variable_get('ckan_api_url', 'http://www.ckan.net/');
//    $uname = variable_get('ckan_htauth_user', '');
//    $pword = variable_get('ckan_htauth_pass', '');
    $api_key = variable_get('ckan_api_key', '');
    
//    $ckan_connector = new CkanConnector($url, $uname, $pword, $api_key);
    $ckan_connector = new CkanConnector($url, $api_key);
  }
  
  return $ckan_connector;
}

/**
 * retrieve all key-value pairs for a given node.
 * 
 * @param int $nid
 */
function ckan_read_dictionary($nid) {
  if (empty($nid)) return;
  
  // initialise
  $output = array();

  // extract all keyvals from db table
  $result = db_query("SELECT name, value FROM {ckan_dictionary} c WHERE c.nid = %d", $nid);
  
  while ($row = db_fetch_object($result)) {
    // add row data to output
    $output[$row->name] = unserialize($row->value);
  }
  
  return $output;
}

/**
 * write all key-value pairs for a given node to db.
 * 
 * @param int $nid
 */
function ckan_write_dictionary($node) {
  if (empty($node->ckan_dictionary)) return;

  $keys = ($node->is_new) ? array() : array('nid', 'name');
  
  foreach ($node->ckan_dictionary as $key => $value) {
    $record = (object) array(
      'nid' => $node->nid,
      'name' => $key,
      'value' => $value,
    );
    drupal_write_record('ckan_dictionary', $record, $keys);
  }
  
  return $output;
}

/**
 * delete all keyvals or a specific keyval for a given node
 * 
 * @param int $nid
 * @param string $key
 */
function ckan_delete_dictionary($nid, $key = NULL) {
  if (empty($key)) {
    // delete all keyvals for a given node
    $result = db_query("DELETE FROM {ckan_dictionary} WHERE nid = %d", $nid);
  } else {
    // delete a specific keyval
    $result = db_query("DELETE FROM {ckan_dictionary} WHERE nid = %d AND name = '%s'", $nid, $key);
  } 
}

/**
 * get an array of fields used by ckan
 */
function ckan_get_fields() {
  // initialise
  $fields = array();
  
  // since there is currently no better way I extract fields by grabbing the test package.
  $test_name = variable_get('ckan_test_name', '');
  
  if (empty($test_name)) {
    // test package not defined
    drupal_set_message(t('Failed to ascertain ckan field names as the test package is not defined. You will need to set the test package name in the CKAN Integration settings and not fail the service test before field names can be determined.', 'error'));
   
  } else {
    // extract fields from cache to reduce ckan requests
    $cached_fields = cache_get('ckan_fields__' . $test_name, 'cache');
    $fields = $cached_fields->data;
    
    if (empty($fields)) {
      $test_package = ckan_get_package($test_name);
      $fields = _extract_fieldnames($test_package);
      
      cache_set('ckan_fields__' . $test_name, $fields, 'cache', CACHE_TEMPORARY);
    }
  }
  
  return $fields;
}

/**
 * recursively parse a nested collection an extract all keys 
 * into a single array
 * 
 * also, prepend sub-collection key to sub-keys. look, just look 
 * at the output and that'll make sense! ;-)
 * 
 * @param unknown_type $collection
 */
function _extract_fieldnames($collection) {
  // initialise
  $fields = array();
  
  // process the collection
  foreach ($collection as $fieldname => $value) {
    // replace integers with X so variable length arrays can be catered for
    $fieldname = ckan_xify($fieldname);
    
    // if it's a collection, recurse
    if (is_array($value) || is_object($value)) {
      $child_fieldnames = _extract_fieldnames($value);
      
      foreach ($child_fieldnames as $child_fieldname) {
        // append field parent name to all children
        $full_fieldname = $fieldname . '__' . $child_fieldname;
        $fields[$full_fieldname] = $full_fieldname;
      }
      
    } else {      
      // add field name to output
      $fields[$fieldname] = $fieldname;
    }
  }
    
  return $fields;
}
/**
 * helper function that replaces a string representing an integer with an X
 * 
 * @param string $string
 */
function ckan_xify($string) {
  return is_int($string) || $string == '0' ? 'X' : $string;
}

/**
 * extracts the index from a field name when provide with the generic X version
 * 
 * @param string $fieldname
 */
function ckan_get_field_index($fieldname, $xname) {
  // initialise
  $index = FALSE;
  
  if ($fieldname != $xname) {
    // field is part of an array of fields
    
    // get field parts
    $parts = explode('__', $fieldname);
    $xparts = explode('__', $xname);
    
    // the difference will be the index
    $diff = array_diff($parts, $xparts);
    
    // pop it so that it's a number
    $index = intval(array_pop($diff));
  }

  return $index;
}

function ckan_get_form($op, $arg) {
  // initialise
  $output = '';
  $form_classes = array();
  $form_classes[] = 'ckan-form';
  $form_classes[] = 'ckan-form-' . $op;
  
  global $user;
  
  // create connector
  $ckan_connector = ckan_get_connector();
  
  if (!empty($_POST)) {
    // POST from form submission exists
    $data = array(
      'form_data' => $_POST, 
      'author' => "{$user->name} (uid {$user->uid} )",
    );
    
    // post appropriate form
    switch ($op) {
      case 'edit':
        // rename argument to something more useful
        $node = $arg;
        
        $data['log_message'] = 'updating package';
        
        try {
          $output = $ckan_connector->postPackageEditForm($node->ckan_dictionary['ckan_id'], $data, !(user_access('create ckan packages') ? 1 : 0), $user->uid);
          
        } catch (CkanBadRequestException $e) {
          $output = $e->getMessage();
          $form_classes[] = 'has-errors';
          
        } catch (Exception $e) {
          drupal_set_message(check_plain($e->getMessage()), 'error');
        }
        
        if (empty($output)) drupal_goto('node/' . $node->nid);
        break;
        
      case 'create':
        // rename argument to something more useful
        $type = $arg['#type'];
        $type_title = $arg['#title'];
        
        $data['log_message'] = 'creating package';
        
        $form_classes[] = 'ckan-form-' . $type;
        $form_classes[] = 'ckan-form-' . $op . '-' . $type;
        
        try {
          $output = $ckan_connector->postPackageCreateForm($data, $user->uid);
          
        } catch (CkanBadRequestException $e) {
          $output = $e->getMessage();
          $form_classes[] = 'has-errors';
          
        } catch (Exception $e) {
          drupal_set_message(check_plain($e->getMessage()), 'error');
        }
        
        $path = variable_get('ckan_default_base_path', 'ckan/catalogue');
        
        if (module_exists('pathauto')) {
          $path = variable_get('pathauto_node_pattern', $path);
          $path = variable_get("pathauto_node_{$type}_pattern", $path);
          $path = preg_replace('#\[.*?\]#', '', $path);
          $path = (empty($path)) ? $default_node_path : $path;
        }
        
        if (empty($output)) drupal_goto($path . $_POST['Package--name']);
        
        break;
    }    
    
  } else {
    // display appropriate form
    switch ($op) {
      case 'edit':
        // rename argument to something more useful
        $node = $arg;
        
        try {
          $output = $ckan_connector->getPackageEditForm($node->ckan_dictionary['ckan_id'], !(user_access('create ckan packages') ? 1 : 0), $user->uid);
          
        } catch (Exception $e) {
          drupal_set_message(check_plain($e->getMessage()), 'error');
          $form_classes[] = 'has-errors';
        }
        break;
        
      case 'create':
        // rename argument to something more useful
        $type = $arg['#type'];
        $type_title = $arg['#title'];
        
        try {
          $output = $ckan_connector->getPackageCreateForm($user->uid);
          $form_classes[] = 'has-errors';
          $form_classes[] = 'ckan-form-' . $type;
          $form_classes[] = 'ckan-form-' . $op . '-' . $type;
          
        } catch (Exception $e) {
          drupal_set_message(check_plain($e->getMessage()), 'error');
          $form_classes[] = 'has-errors';
        }
        
        break;
    }
  }
  
  $output = '<form method="post" id="ckan-embedded-form" class="' . implode(' ', $form_classes) . '">' . $output . '<input type="submit"></form>';
    
  return $output;
}

  


