<?php
// $Id$

/**
 * @file
 * Taken from the ckan_importer module Ckan class by Sean Burlington,
 * as previously released on data.gov.uk
 * 
 * Not enough time to redo from scratch so i've just tidied it up to meet
 * drupal coding standards. i'm working on a service consumption module 
 * that this could possibly tie into. watch this space
 */

/**
 * An interface to the CKAN API.
 * 
 * Returns data in object format (the output of json_decode)
 *
 */
class CkanConnector {
  private $url = 'http://www.ckan.net/';
  private $pword = '';
  private $uname = '';
  private $no_result_means_error = 1;
  private $no_result_means_array = 2;
  private $errors = array(
    '0'  =>   'Network Error?',
    '301'  =>   'Moved Permanently',
    '400'  =>   'Bad Request',
    '401'  =>   'Authorization Required',
    '403'  =>   'Not Authorized',
    '404'  =>   'Not Found',
    '409'  =>   'Conflict (e.g. name already exists)',
    '500'  =>   'Internal Server Error', 
  );
  
  /**
   * Create an instance with defined url, password etc
   * 
   * @param  $url
   *   the URL of the ckan site
   * @param  $uname
   *   optional username if htauth is being used
   * @param  $pword
   *   optional password if htauth is being used
   * @param  $apikey
   *   optional api key if htauth is not used
   *   only required for saving data to ckan
   */
  public function __construct($url = NULL, $apikey = '') {
    if ($url) {
      $this->url=$url;
    }
    $this->apikey= $apikey;
  }
  
  /**
   * Manages all tranfer of data - used by the other functions.
   * 
   * @param  $url
   *    The path foir this request (not including the domain)
   * @param  $no_result_meaning
   *    whether to treat no result as an error, an array or other in the future 
   * @param  $post
   *    Does this request need to post data?
   * @param  $data
   *   data to be transfered.
   */
  private function transfer($url, $no_result_meaning = 1, $post = FALSE, $data = NULL, $nojson = FALSE) {
    $ch = curl_init(check_url($this->url . $url));
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HEADERFUNCTION, array(&$this, 'readHeader'));
    
    if ($this->uname && $this->pword) {
      curl_setopt($ch, CURLOPT_USERPWD, "{$this->uname}:{$this->pword}");
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }
    
    if ($post && $data) {
      curl_setopt($ch, CURLOPT_POST, TRUE);
      
      if ($nojson) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      } else {
        $data = urlencode(json_encode($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data . '\n');
      }
       
      if ($this->apikey) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-CKAN-API-Key: ' . $this->apikey));
      }
    } 
       
    $result = curl_exec($ch);
    $info = curl_getinfo($ch);
    
    $this->responseHeader = $info['http_code'];
    
    curl_close($ch);
    
    switch ($info['http_code']) {
      case 403:
        throw new CkanNotAuthorizedException('403 : ' . $this->errors[403]);
        break;
        
      case 403:
        throw new CkanBadRequestException($result);
        break;
        
      default:
        if ($info['http_code'] != 200 && $info['http_code'] != 201) {
          throw new CkanException($info['http_code'] . ' : ' . $this->errors[$info['http_code']]);
        }
    }

    if (!$result && $no_result_meaning == $this->no_result_means_error) {
      throw new CkanException("No Result");
      
    } elseif (!$result and $no_result_meaning == $this->no_result_means_array) {
      $result = '[]';
    }
    
    return ($nojson) ? $result : json_decode($result);
  }
  /**
   * Get CKAN serach results
   * @param  $keyword
   */
  public function search($keyword) {
    $results = $this->transfer('api/search/package/?all_fields=1&q=' . urlencode($keyword));
    
    if (!$results->count) {
      throw new CkanException("Search Error");
    }
    return $results;
  }
  /**
   * Get a CKAN package
   * @param  $package
   */
  public function getPackage($package) {
    try {
      $package = $this->transfer('api/rest/package/' . urlencode($package));
    
    } catch (CkanNotAuthorizedException $e) {
      // packages in deleted state return a 403
      throw new CkanPackageDeletedException($e->getMessage());
    }

    if (!$package->name) {
      throw new CkanException("Package Load Error");
    }
    return $package;
  }

  /**
   * Update a package.
   * 
   * @param  $package_name
   * @param  $package
   *    the package in object format
   */
  public function updatePackage($package_name, $package) {
    $package = $this->transfer('api/rest/package/' . urlencode($package_name), 1, TRUE, $package);
    return $package;
  }
  
  /**
   * Create a new package.
   * @param  $package_name
   * @param  $package
   * the package in object format
   */
  public function createPackage($package_name, $package) {
    $package = $this->transfer('api/rest/package' , 1, TRUE, $package);
    return $package;
  }
  
  /**
   * Get a list of all packages.
   */
  public function getPackageList() {
    $list =  $this->transfer('api/rest/package');
    if (!is_array($list)) {
      throw new CkanException("Package List Error");
    }
    return $list;
  }
  /**
   * Get the group response from ckan.
   * 
   * @param  $group
   */
  public function getGroup($group) {
    $group = $this->transfer('api/rest/group/' . urlencode($group) );
    if (!$group->name) {
      throw new CkanException("Group Error");
    }
    return $group;
  }

  /**
   * Get revisonlist since the specified time.
   * @param  $time
   *   in unix time
   */
  public function getRevisionsSinceTime($time) {
    
     $time = strftime("%Y-%m-%dT%H:%M:%S", $time);
    $revisionList = $this->transfer('api/search/revision?since_time=' . $time, $this->no_result_means_array );
    return $revisionList;
  }

  /**
   * Get revisonlist since revisionID.
   * @param  $revisionID
   *   
   */
  public function getRevisionsSinceRevision($revisionID) {
     
    $revisionList = $this->transfer('api/search/revision?since_id=' . $revisionID, $this->no_result_means_array );
    return $revisionList;
  }
  /**
   * Get a revison 
   * @param  $revisionID
   * @return $revision
   *   contains timestamp, log message, and list of package affected.
   */
  public function getRevision($revisionID) {
    $revision = $this->transfer('api/rest/revision/' . urlencode($revisionID) );
    return $revision;
  }
  
  /**
   * Get the licenselist.
   * 
   */
  public function getLicenseList() {
    $list =  $this->transfer('api/rest/licenses');
    if (!is_array($list)) {
      throw new CkanException("License List Error");
    }
    return $list;
  }

  /**
   * Get a package edit form.
   * 
   */
  public function getPackageEditForm($packageID, $restrict = 0) {
    $html =  $this->transfer('api/form/package/edit/' . $packageID . ($restrict ? '?restrict=1' : ''), 1, FALSE, NULL, TRUE);
    return $html;
  }
  
  /**
   * Submit a package edit form
   */
  public function postPackageEditForm($packageID, $post, $restrict = 0) {
    $package = $this->transfer('api/form/package/edit/' . $packageID . ($restrict ? '?restrict=1' : ''), 1, TRUE, $post);
    return $package;
  }
  
  /**
   * Get a package creation form.
   * 
   */
  public function getPackageCreateForm($uid = NULL) {
    $html =  $this->transfer('api/form/package/create' . ($uid ? '?user_id=' . $uid : ''), 1, FALSE, NULL, TRUE);
    return $html;
  }
  
  /**
   * Submit a package creation form
   */
  public function postPackageCreateForm($post, $uid = NULL) {
    $package = $this->transfer('api/form/package/create' . ($uid ? '?user_id=' . $uid : ''), 1, TRUE, $post);
    return $package;
  }
  

  
  
  
  /**
   * CURL callback function for reading and processing headers
   *  
   * @param object $ch
   * @param string $header
   * @return integer
   */
  private function readHeader($ch, $header) {
    //extracting example data: filename from header field Content-Disposition
    $location = $this->extractCustomHeader('Location: ', '\n', $header);
    $harvest_source = $this->extractCustomHeader('\/api\/rest\/harvest\/source\/', '\n', $header);
    if ($location) {
      $this->response_meta_info['location'] = trim($location);
    }
    if ($harvest_source) {
      $this->response_meta_info['harvest_source'] = trim($harvest_source);
    }
    return strlen($header);
  }

  private function extractCustomHeader($start, $end, $header) {
    $pattern = '/'. $start .'(.*?)'. $end .'/';
    if (preg_match($pattern, $header, $result)) {
      return $result[1];
    } else {
      return FALSE;
    }
  }
  
  function getHeaders() {
    return $this->response_meta_info;
  }
  
}


class CkanException extends Exception{}

class CkanNotAuthorizedException extends CkanException{}

class CkanPackageDeletedException extends CkanException{}

class CkanBadRequestException extends CkanException{}