<?php
// $Id$

/**
 * @file
 * Admin settings
 * 
 * This file defines the admin settings and any code specifically used for this purpose 
 */

/**
 * implementation of hook_form_alter()
 * 
 * @param array $form
 * @param array $form_state
 * @param string $form_id
 */
function ckan_ui_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'ckan_settings_form':
      // add setting for advanced theming
      $form['fs-general']['ckan_theming_type']['#options'][2] = 'Advanced theming';
      $form['fs-general']['ckan_theming_type']['#description'] .= ' CKAN UI module adds Advanced theming option.';
      
      // add setting to suppress blank values
      $form['fs-general']['ckan_ui_show_blank_values'] = array(
        '#type' => 'select',
        '#title' => t('Show blank values on node page'),
        '#options' => array(
          '0' => 'No - hide blank values',
          '1' => 'Yes',
        ),
        '#default_value' => variable_get('ckan_ui_show_blank_values', FALSE),
        '#description' => t('Use this setting to hide or show blank fields when viewing ckan-enabled node pages.'),
      );      
      break;
  }
}

/**
 * settings form to configure ckan fields. there will be a different form 
 * for each ckan-enabled content type
 * 
 * @param array $form_state
 * @param string $type
 */
function ckan_ui_settings_form($form_state, $type) {
  $form = array();
  
  $fields = ckan_get_fields();
  $test_name = variable_get('ckan_test_name', '');
  
  if (empty($test_name)) {
    // test package not defined
    drupal_set_message(t('Before you can customise the UI, you will need to set the test package name in the CKAN Integration settings and not fail the service test.'), 'error');
    
  } else {
  
    $form['fs-ckan_groups'] = array(
      '#type' => 'fieldset',
      '#title' => t('Group settings'),
      '#weight' => -200,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );              
    $form['fs-ckan_groups']['ckan_ui_group_labels'] = array(
      '#type' => 'textarea',
      '#title' => t('Group labels'),
      '#default_value' => variable_get('ckan_ui_group_labels', ''),
      '#description' => t('List groups on a new line in the format: name|label. Name is used in the field settings to identify which group the field belongs to'),
    );
    
    // display fields form
    $field_cnt = -100;
    
    foreach ($fields as $field) {
  
      $form["fs-ckan_field_{$field}"] = array(
        '#type' => 'fieldset',
        '#title' => t('Field settings: ' . $field),
        '#weight' => $field_cnt,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );        
      $form["fs-ckan_field_{$field}"]["ckan_ui_field_label_{$field}"] = array(
        '#type' => 'textfield',
        '#title' => t('Label for !field', array('!field' => $field)),
        '#default_value' => variable_get("ckan_ui_field_label_{$field}", $field),
        '#description' => t('This is the label used when this field is themed.'),
      );
      $form["fs-ckan_field_{$field}"]["ckan_ui_field_show_{$field}"] = array(
        '#type' => 'checkbox',
        '#title' => t('Show !field', array('!field' => $field)),
        '#default_value' => variable_get("ckan_ui_field_show_{$field}", TRUE),
        '#description' => t('Display this field when viewing the data package.'),
      );
      $form["fs-ckan_field_{$field}"]["ckan_ui_field_group_{$field}"] = array(
        '#type' => 'textfield',
        '#title' => t('Group for !field', array('!field' => $field)),
        '#default_value' => variable_get("ckan_ui_field_group_{$field}", ''),
        '#description' => t('Display this field in a subgroup. Value be machine-readable. Define possible values in Group Settings above.'),
      );
      $form["fs-ckan_field_{$field}"]["ckan_ui_field_weight_{$field}"] = array(
        '#type' => 'textfield',
        '#title' => t('Weight for !field', array('!field' => $field)),
        '#default_value' => variable_get("ckan_ui_field_weight_{$field}", 0),
        '#description' => t('Fields with lower weights appear higher within the group.'),
      );
      
      $field_cnt++;
    }
  }
  
  return system_settings_form($form);
}

