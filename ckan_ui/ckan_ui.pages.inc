<?php 
// $Id$
/**
 * @file
 * Theming functions 
 * 
 * This contains all functions relating to advanced theming for 
 * the CKAN integration module.
 * 
 */

/**
 * implementation of hook_theme
 *
 * @return array
 */
function ckan_ui_theme() {
  return array(
    'ckan_fieldgroups' => array(
      'arguments' => array('fieldgroups' => array()),
    ),
    'ckan_fieldgroup' => array(
      'arguments' => array('fieldgroup' => array(), 'name' => NULL, 'title' => NULL),
      'template' => 'ckan_fieldgroup',
    ),
    'ckan_field' => array(
      'arguments' => array('field' => array()),
      'template' => 'ckan_field',
    ),
  );
}

/**
 * implementation of hook_theme_registry_alter
 * 
 * @param $theme_registry
 */
function ckan_ui_theme_registry_alter(&$theme_registry) {
  // add theme directory to theme path suggestions 
  // so that custom template overrides are found
  array_unshift($theme_registry['ckan_fieldgroup']['theme paths'], path_to_theme());
  array_unshift($theme_registry['ckan_field']['theme paths'], path_to_theme());
}

/**
 * theme function for a collection of ckan_groups
 * 
 * @param array $items
 */
function theme_ckan_fieldgroups($fieldgroups) {
  // initialise
  $output = '';
  
  if (!empty($fieldgroups)) {
    drupal_add_css(drupal_get_path('module', 'ckan_ui') .'/css/ckan_ui.css');
    
    // get field groups from settings
    $fieldgroups_settings = ckan_ui_get_fieldgroups();
    
    foreach ($fieldgroups_settings as $fieldgroup_name => $fieldgroup_title) {
      if (!empty($fieldgroups[$fieldgroup_name])) {
        $output .= theme('ckan_fieldgroup', $fieldgroups[$fieldgroup_name], $fieldgroup_name, $fieldgroup_title);
      }
    }
  }
  
  return $output;
}

/**
 * preprocess function for ckan_group
 * 
 * @param array $variables
 */
function template_preprocess_ckan_fieldgroup(&$variables) {
  // add contextual node to variables
  $variables['node'] = menu_get_object();
  $variables['ckan_fieldgroups'] =& $variables['node']->ckan_flat;
  
  // determine css classes
  $classes = array(
    'ckan-fieldgroup',
    'ckan-fieldgroup--' . $variables['name'],
  );
  $variables['classes'] = implode(' ', $classes);
  
  $field_classes = array(
    'ckan-field',
    'ckan-field--' . $field['#name'],
  );
  $variables['field_classes'] = implode(' ', $field_classes);   

  // list extra template suggestions for greater customisation
  $variables['template_files'][] = 'ckan_fieldgroup--' . $variables['name'];
  
  // create preprocess suggestions
  global $theme_key;
  
  $function_name = $theme_key . '_preprocess_ckan_fieldgroup__'. $variables['name'];
  if (function_exists($function_name)) {
    $function_name($variables);
  }
  
  // intialise
  $variables['content'] = '';
  
  // theme each field in the group and add to content
  foreach ($variables['fieldgroup'] as $field) {
    $variables['content'] .= theme('ckan_field', $field);
  }
  
}

/**
 * preprocess function for ckan_item
 * 
 * @param array $variables
 */
function template_preprocess_ckan_field(&$variables) {
  $field = $variables['field'];
  
  // list extra template suggestions for greater customisation
  $variables['template_files'][] = 'ckan_field--' . $field['#name'];
  
  // set template data
  $variables['title'] = $field['#title'];
  $variables['description'] = $field['#value'];
 
  // determine css classes
  $classes = array(
    'ckan-field',
    'ckan-field--' . $field['#name'],
  );
  $variables['classes'] = implode(' ', $classes); 
     
  // add contextual node information to variables
  $variables['node'] = menu_get_object();
  $variables['ckan_fieldgroups'] =& $variables['node']->ckan_flat;
  
  // create preprocess suggestions
  global $theme_key;
  
  $function_name = $theme_key . '_preprocess_ckan_fieldgroup__'. $variables['name'];
  if (function_exists($function_name)) {
    $function_name($variables);
  }
}
